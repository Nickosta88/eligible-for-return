<?php

declare(strict_types=1);

namespace Elogic\EligibleForReturn\Model\Resolver;

use Elogic\EligibleForReturn\Helper\Config;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Stdlib\ArrayManager;

class Resolver implements ResolverInterface
{
    private Config $config;

    private ArrayManager $arrayManager;


    public function __construct(
        Config $config,
        ArrayManager $arrayManager
    ) {
        $this->config = $config;
        $this->arrayManager = $arrayManager;
    }

    /**
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return \Magento\Framework\GraphQl\Query\Resolver\Value|mixed|string|null
     */
    public function resolve( Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        $product = $this->arrayManager->get('model', $value);
        if (is_null($product)) {
            return null;
        }

        $attribute = $product->getCustomAttribute('eligible_for_return');
        if (is_null($attribute)) {
            return null;
        }

        $eligibleForReturn = (int)$attribute->getValue();
        if (!$eligibleForReturn) {
            return null;
        }

        return $this->config->getTemplateAndRender($eligibleForReturn);
    }
}
