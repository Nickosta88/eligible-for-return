<?php
declare(strict_types=1);
namespace Elogic\EligibleForReturn\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class EligibleForReturnAttribute implements DataPatchInterface
{

    /** @var ModuleDataSetupInterface */
    private ModuleDataSetupInterface $moduleDataSetup;

    /** @var EavSetupFactory */
    private EavSetupFactory $eavSetupFactory;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }


    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $attributeCode = 'eligible_for_return';
        $sortOrder = 31;
        $eavSetup->addAttribute(
            Product::ENTITY,
            $attributeCode,
            [
                'group' => 'General',
                'type' => 'int',
                'label' => 'Eligible for return',
                'is_global' => true,
                'visible' => true,
                'required' => false,
                'unique' => false,
                'sort_order' => $sortOrder,
                'is_used_in_grid' => false,
                'is_filterable_in_grid' => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'visible_in_advanced_search' => true,
                'searchable' => true,
                'filterable' => false,
                'user_defined' => true,
                'is_visible_on_front' => true,
                'wysiwyg_enabled' => true,
                'is_html_allowed_on_front' => true
            ]
        );
        $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);

    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases(): array
    {
        return [];
    }
}
